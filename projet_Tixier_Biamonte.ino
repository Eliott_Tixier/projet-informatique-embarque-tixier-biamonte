#include <Bounce.h>

// Variables globales
const int channel = 1;
const int nbBouton = 3;

Bounce boutons[nbBouton] =
{
  Bounce (1, 5),
  Bounce (2, 5),
  Bounce (3, 5)
};

int btnChanel[nbBouton] = {20, 21, 22};
int btnActiver[nbBouton] = {0, 0, 0};

void setup() {
  for (int i = 1; i <= nbBouton ; i++) {
    pinMode(i, INPUT_PULLUP);
  }
}

void loop() {
  // Mise à jours de l'état des boutons. (Ne pas mettre de delay sinon nous augmentons le temps de mis à jour)
  for (int i = 0; i < nbBouton; i++) {
    boutons[i].update();
  }

  for (int i = 0; i < nbBouton; i++) {
    if (boutons[i].risingEdge() && btnActiver[i] == 0) {
      usbMIDI.sendControlChange(btnChanel[i], 127, channel);
      btnActiver[i] = 1;
    } else if (boutons[i].risingEdge() && btnActiver[i] == 1) {
      usbMIDI.sendControlChange(btnChanel[i], 0, channel);
      btnActiver[i] = 0;
    }
  }

  // MIDI Controllers should discard incoming MIDI messages.
  // http://forum.pjrc.com/threads/24179-Teensy-3-Ableton-Analog-CC-causes-midi-crash
  while (usbMIDI.read()) {
    // ignore incoming messages
  }
}
